# Gitlab Docker Template

A boilerplate for deploying a docker image on a self-hosted Gitlab instance.

## Install Gitlab Omnibus on Debian 10

### Setup the DNS record
Add an A record to your dns server for gitlab.examplecompany.com
Ensure that the address can be resolved by all clients and runners

### Install Gitlab
Following the directions on the gitlab website [here](https://about.gitlab.com/install/#debian).  

Update and install dependencies.
```console
sudo apt-get update
sudo apt-get install -y curl openssh-server ca-certificates perl
```

Install Postfix mail service, select local for Postfix Configuration
```console
sudo apt-get install -y postfix
```

Add the repository using the debian script.
```console
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
```

Install the package.
```console
sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee
```

### Create self signed certificates with DNS alternate names.

Self signed certificates are challenging with Gitlab, the details are [here](https://docs.gitlab.com/omnibus/settings/ssl.html)

Create a directory
```console
mkdir ~/certs && cd ~/certs
```

Create a key for the ca
```console
openssl genrsa -out ca.key 2048
```

Generate CA certificates
```console
openssl req -x509 -new -nodes -key ca.key -subj "/CN=ExampleCompany/C=CA/L=Ontario" -sha256 -days 1825 -out ca.crt
```

Create the server private key
```console
openssl genrsa -out server.key 2048
```

Create a new file that contains the details for the server certificates
```console
nano ~/certs/csr.conf
```

Contents of ~/certs/csr.conf
```console
[ req ]
default_bits = 2048
prompt = no
default_md = sha256
x509_extensions    = v3_req
distinguished_name = dn

[ dn ]
C = CA
ST = Ontario
L = Guelph
O = ExampleCompany
OU = ExampleCompany Division
CN = gitlab.examplecompany.com

[ v3_req ]
subjectAltName = @alt_names

[ alt_names ]
DNS.1 =  gitlab.examplecompany.com
IP.1 = 10.0.0.3
```

Generate the certificate signing request (CSR)
```console
openssl req -new -key server.key -out server.csr -config csr.conf
```

Verify the CSR 
```console
openssl req -text -noout -verify -in server.csr
```

Generate the server certificate, make note of the -extensions flag, these are required or you will get a SAN's error when using the docker registry.
```console
openssl x509 -req -in server.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out server.crt -days 3650 -extensions v3_req -extfile csr.conf
```

Paste the Root CA to the end of the server certificate
```console
-----BEGIN CERTIFICATE-----
[Server Certificate Here]
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
[CA Certificate Here]
-----END CERTIFICATE-----
```

Verify the certificate using Gitlab's compiled openssl.  Be certain to verify the alternate name is present.
```console
/opt/gitlab/embedded/bin/openssl x509 -in server.crt -text -noout
```

### Setup the Self-Signed certificate

Copy the server.crt and server.key to the gitlab ssl directory
```console
cp ~/certs/server.key /etc/gitlab/ssl/gitlab.examplecompany.com.key
cp ~/certs/server.crt /etc/gitlab/ssl/gitlab.examplecompany.com.crt
```

Enable the self-signed certificate
```console
nano /etc/gitlab/gitlab.rb
```

```console
...
# https indicates we are using certs
external_url "https://gitlab.example.com"
...

...
# If it is not a letsencrypt, it must be self signed
letsencrypt['enable'] = false
...
```

Reconfigure Gitlab
```console
gitlab-ctl reconfigure
systemctl reboot
```

### First Login

Retrieve the randomly generated root password
```console
nano /etc/gitlab/initial_root_password
```

Navigate your browser to https://gitlab.examplecompany.com

### Enabling the Docker Registry

Uncomment the registry address and update the port in the gitlab.rb configuration file
```console
nano /etc/gitlab/gitlab.rb
```

```console
...
registry_external_url 'https://gitlab.examplecompany.com:5050'
...
```

Reconfigure Gitlab
```console
gitlab-ctl reconfigure
```

### Setup a Runner - Shell Type
For this use case the runner is a seperate Debian 10 installation

Install Docker on the host

Add the ca.crt to the trusted CA...
Make a directory for the CA certificate on the runner host
```console
sudo mkdir /usr/local/share/ca-certificates/gitlab
```

From the gitlab server copy the ca.crt to the runner
```console
sudo scp ~/certs/ca.crt username@runnerIP:~
sudo mv ~/ca.crt /usr/local/share/ca-certificates/gitlab/
```

Update the list of trusted CA certificates
```console
sudo update-ca-certificates
```

Add the reposititory
```console
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```

Install the package using apt
```console
sudo apt update
sudo apt-get install gitlab-runner
```

Add the gitlab-runner user to the docker users group
```console
sudo usermod -aG docker gitlab-runner
```

Register the runner, add the tag 'linux' when registering to be compatible with the example CI configuration.
```console
sudo gitlab-runner register --url "https://gitlab.examplecompany.com/" --registration-token "PROJECT_REGISTRATION_TOKEN"
```
### Install the build environment requirements on the runner
If you are building with python install python and pip
```console
sudo apt update
sudo apt install python3 python-pip3
```

If you are buiding with Node.js install node and npm
```console
sudo apt update
sudo apt install nodejs npm
```

etc...

### Creating a Docker Container
Create a new project and copy the two files from this repository, Dockerfile and .gitlab-ci.yml.  The Dockerfile is will be used to create the docker image, and the .gitlab-ci.yml file will be used to build and push the container to Gitlab's built in container registry.

### Troublshooting

If you get an error X509 not allowed when interacting with runner and docker registry.  You can use a fullchain.pem file to avoid the problem.  Make the fullchain.pem file by pasting the server.key, server.crt and ca.crt into on file in that order.

```console
sudo gitlab-runner register --url https://gitlab.examplecompany.com/ --registration-token "somelongtoken" --tls-ca-file ~/fullchain.pem
```

For strange console errors comment out the console clear at logout for the gitlab-runner user
```console
sudo nano /home/gitlab-runner/.bash_logout
```
